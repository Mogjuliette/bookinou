import {React, useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import HomeScreen from './src/HomeScreen';
import StoryScreen from './src/StoryScreen';


// Initialize Apollo Client
const client = new ApolloClient({
  uri: 'https://countries.trevorblades.com/graphql', 
/*   uri: 'https://api-v4.1-dev.mybookinou.com/api/graphql',
 */  cache: new InMemoryCache()
});



export default function App() {

  const [toggle, setToggle] = useState(false);

  return (
    <ApolloProvider client={client}>
    <View style={styles.container}>
        <Button
          onPress={() => setToggle(!toggle)}
          title="Résultats GraphQL Continents"
          color="#841584"
          accessibilityLabel="A propos de ce bouton"
        />
      {toggle && <div>
        <Text style={styles.title}>World continents</Text>
        <HomeScreen/>
      </div>}
      {/*   */}
    </View>
    </ApolloProvider>
  );
}

export const styles = StyleSheet.create({
  container: {
    paddingTop:40,
    paddingLeft:30,
    paddingRight: 30,
    flex:1,
    flexWrap : 'wrap',
    alignContent:'center',
    justifyContent:'space-around',
  },
  item: {
    paddingTop: 16,
    paddingBottom: 16,
    paddingLeft: 20,
    paddingRight: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  header: {
    fontWeight: 'bold',
    fontSize: 16
  },
  title:{
    fontWeight: 'bold',
    fontSize: 20
  },
  modalView: { 
    width:'90%',
    height:'50%',
    backgroundColor: "white",
    borderRadius: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    paddingBottom:10
  },
  button: {
    borderRadius: 10,
    padding: 5,
    elevation: 2,
    backgroundColor: "lightgrey"
  },
});
