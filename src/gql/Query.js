import { gql } from "@apollo/client";

export const CONTINENT_QUERY = gql`
  query ContinentQuery {
    continents {
      code
      name
    }
  }
`;
/* 
export const StORY_QUERY = gql`
{
  stories {
    edges {
      node {
        id
        title
        picture {
          id
          contentUrl
        }
      }
    }
  }
}
`; */