import { Text, FlatList, Pressable} from 'react-native'
import { useQuery } from "@apollo/client";
import { CONTINENT_QUERY } from "./gql/Query";
import { styles } from "../App";

export default function HomeScreen() {



const { data, loading } = useQuery(CONTINENT_QUERY); 
  
    const ContinentItem = ({ continent }) => {
      const { name } = continent; 


  
      return (
        <Pressable style={styles.item}>
          <Text style={styles.header}>{name}</Text> 
        </Pressable>
      );
    };
  
    if (loading) {
      return <Text>Fetching data...</Text> 
    }
  
    return (
      <>
      
        {<FlatList
          data={data.continents}
          renderItem={({ item }) => <ContinentItem continent={item} />}
          keyExtractor={(item, index) => index}
        />}
      
    </>
    );
  }

