/* import { Text, FlatList, Pressable} from 'react-native'
import { useQuery } from "@apollo/client";
import { STORY_QUERY } from "./gql/Query";
import { styles } from "../App";


export default function StoryScreen() {



const { data, loading } = useQuery(STORY_QUERY); 
  
    const StoryItem = ({ story }) => {
      const { title, id } = story; 
  
      return (
        <Pressable style={styles.item}>
          <Text style={styles.header}>{title}</Text> 
        </Pressable>
      );
    };
  
    if (loading) {
      return <Text>Fetching data...</Text> 
    }
  
    return (
      <>
      
        {<FlatList
          data={data.story}
          renderItem={({ item }) => <StoryItem story={item} />}
          keyExtractor={(item, index) => index}
        />}
      
    </>
    );
  }

 */